
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
var ena;
var dva; 
var tri;

function generirajPodatke(stPacienta) {
   var ehrId = "";
    // TODO ustvari random podatke
	
	generate(stPacienta);	
	//ehrId = NOOB;
	setTimeout(function(){	
	

		console.log("generirajPodatke " + ehrId);
  if(stPacienta == 1){
  	//normalen pacient
  	ehrId = NOOB;
  	ena = ehrId;
  	var podatki = {
  		telesnaTeza: [75, 80, 82, 79, 76, 81, 72, 75, 80],
  		telesnaVisina: [185],
  		sistolicniKrvniTlak: [130],
  		diastolicniKrvniTlak: [105]
  	};
  	
  	dodajTeznePodatke(ehrId, podatki);
  }else if(stPacienta == 2){
  	//predebel pacient
  	ehrId = NOOB2;
  	dva = ehrId;
  		var podatki = {
  		telesnaTeza: [120, 110, 115, 112, 123, 118, 120, 119, 121],
  		telesnaVisina: [185],
  		sistolicniKrvniTlak: [150],
  		diastolicniKrvniTlak: [135]
  	};
  	
  	dodajTeznePodatke(ehrId, podatki);
  }else{
  	//presuh pacient
  	ehrId = NOOB3;
  	tri = ehrId;
  		var podatki = {
  		telesnaTeza: [57, 62, 58, 57, 58, 59, 61, 56, 53],
  		telesnaVisina: [185],
  		sistolicniKrvniTlak: [120],
  		diastolicniKrvniTlak: [94]
  	};
  	
  	dodajTeznePodatke(ehrId, podatki);
  }
  setTimeout(function() {
	console.log("Glavni ehrId:" + ehrId);
	return ehrId;    
  }, 500);
  
	}, 600);
  // TODO: Potrebno implementirati
  
  
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

var visina;


//kreitanje novega pacienta
function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();
    
	var ime = $("#ime").val();
	var priimek = $("#priimek").val();
    var datumRojstva = $("#datum").val() + "T00:00:00.000Z";
    

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        console.log("Hej");
		        var partyData = {
		            firstNames: ime, 
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#result").append("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                         //Če kreiraš novega pacienta, potem naj se doda med možnosti
                          var select = document.getElementById("preberiObstojeciEHR");
                          var opt = document.createElement('option');
    						opt.value = ehrId;
    						opt.innerHTML = ime + " " + priimek;
    						select.appendChild(opt);
		                	 return ehrId;
		                	
		                }else{
		                    console.log("Napaka");
		                }
		            },
		            
		        });
		    }
		});
	
}





//Podatki o težah pacienta
var teze = [];

//Funkcija, ki vrača osnovne podatke in teže posameznih pacientov
function vrniPacienta(ehrPacienta){
    var sessionId = getSessionId();
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
    /*
    $.ajax({
        url: baseUrl + "/demographics/party/query",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(searchData),
        success: function (res) {
            
            $("#header").html("Search by ");
            for (i in res.parties) {
                var party = res.parties[i];
                $("#result").append(party.firstNames + ' ' + party.lastNames + '<br>');
            }
        }
    });
    */
    //Vrni podatke o teži
    $.ajax({
    url: baseUrl + "/view/" + ehrPacienta + "/weight",
    type: 'GET',
    success: function (res) {
        for (var i in res) {
            /*$("#result").append(res[i].weight + " " +  res[i].unit + "<br>");*/
            teze.push(res[i].weight);
        }
        console.log(teze);
    }
    
	});
	//Vrni podatke o višini
    $.ajax({
    url: baseUrl + "/view/" + ehrPacienta + "/height",
    type: 'GET',
    success: function (res) {
        for (var i in res) {
            visina = res[i].height;
        }
       
    }
    });
}


var counter = 0;
var NOOB;
var NOOB2;
var NOOB3;
//Funkcija, ki kreira pacienta za generiranje testnih pacientov
function kreirajTestniPrimer(ime, priimek, datumRojstva) {
	var sessionId = getSessionId();
    
	var ime = ime;
	var priimek = priimek;
    var datumRojstva = datumRojstva + "T00:00:00.000Z";
    var resitev = 0;

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime, 
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        if(counter % 3 == 0){
		        	NOOB = ehrId;	
		        }else if(counter % 3 == 1){
		        	NOOB2 = ehrId;
		        }else{
		        	NOOB3 = ehrId;
		        }
		        counter++;
				
				//data = data.ehrId;
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            //async: false,
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                   /* $("#result").append("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                          */console.log("Moj je:" + ehrId);
            				
		                }else{
		                    console.log("Napaka");
		                }
		            },
		            
		        });/*
		        console.log("Bam " + data);
		        return data;
		    */}
		});
		
	
	
}

function generate(stevilo){
    if(stevilo == 1){
    	console.log(stevilo);
    	var a = "";
    	
    	kreirajTestniPrimer("Janez", "Novak", "1992-02-03");
    	setTimeout(function(){}, 1000);
    
    }else if(stevilo == 2){
        kreirajTestniPrimer("Karl", "Erjavec", "1978-09-05");
       setTimeout(function(){}, 1000);
    }else{
       kreirajTestniPrimer("Hasan", "Ibn Saba", "1964-12-07");
setTimeout(function(){}, 1000);
    }
}

//Za testne primere -> za generiranje pacientov
function dodajTeznePodatke(ehrId, seznamPodatkov){
	sessionId = getSessionId();
	
	var telesnaTeza = seznamPodatkov.telesnaTeza;
	var telesnaVisina = seznamPodatkov.telesnaVisina;
	var sistolicniKrvniTlak = seznamPodatkov.sistolicniKrvniTlak;
	var diastolicniKrvniTlak = seznamPodatkov.diastolicniKrvniTlak;
	
	//Podatki so v tabeli, to pomeni, da moram iti iterativno čez vse podatke
	//in jih vsakič dodati v tabelo
	for(var i = 0; i  < seznamPodatkov.telesnaTeza.length; i++){

	if (!ehrId|| ehrId.trim().length == 0) {

	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina[0],
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza[i],
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak[0],
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak[0],
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		       console.log("Podatki za pacienta " + ehrId + "so bili znova ustvarjeni");
		    },
		    error: function(err) {
		    	console.log("Prislo je do napake");
		    }
		});
	
	}
	}
}

var ime1;
var ime2;
var ime3;

var stevec = 0;

function vrniIme(ehrId){
	sessionId = getSessionId();
	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
   
    success: function (data) {
        var party = data.party;
        console.log(party.firstNames + " " + party.lastNames);
        if(stevec % 3 == 0){
        	ime1 = party.firstNames + " " + party.lastNames;
        }else if(stevec % 3 == 1){
        	ime2 = party.firstNames + " " + party.lastNames;
        }else{
        	ime3 = party.firstNames + " " + party.lastNames;
        }
        stevec++;
        //return party.firstNames + " " + party.lastNames;
    }
});

}

//Vnesi podatke o pacientu
function dodajMeritveVitalnihZnakov() {
    
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val() + "T11:40Z";
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		console.log(ehrId);
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'Belinda Nurse'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "Podatki so bilo uspešno dodani" + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}
//Prebiranje EHR vrednosti -> uporaba za poljubnega pacienta
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#podatkiOPacientu").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


//Posodabljanje value vrednosti -> kasnejša obravnava za generiranje podatkov
window.onload = function() {
	
	

	//document.getElementById("centralni").style.border = "dotted";
	document.getElementById("levi").style.border = "solid";
	document.getElementById("desni").style.border = "solid";
	/*
	function naloziEhrId(){
		var izbira = document.getElementById("preberiObstojeciEHR");
		var ehr = izbira.value;
		console.log(ehr);
	}*//* Bilo zgolj za teste, zdaj se implementiral master/detail
	document.getElementById("dobiEhr").addEventListener("click", function(){
		var izbira = document.getElementById("preberiObstojeciEHR");
		var ehr = izbira.value;
		document.getElementById("rezultatPoizvedbeOImenu").innerHTML = ehr;
	});*/
	//document.getElementById("vrniPacienta").addEventListener("click", 
	function zavrniPacienta(ehr){
		if (!ehr || ehr.trim().length == 0) {
			$("#zavrni").html("<span class='obvestilo label label-warning " +
    	"fade-in'>Prosim vnesite zahtevan podatek!");
			}else{
		/*var izbira = document.getElementById("preberiObstojeciEHR");
		var ehr = izbira.value;*/
		//vrniPacienta("a9509986-d5d1-466d-8b96-fe83c59707af");
		vrniPacienta(ehr);
		//Nastavimo da počaka 100 ms, preden izvede risanje grafa, 
		//ker hočemo da dobimo teže preden jih narišemo na graf
		setTimeout(function(){
			teze.unshift('Mase');
			console.log(teze);
			var chart = c3.generate({
    			bindto: '#chart',
    			data: {
    				columns: [
    					teze
    				]
    			}	
			});
			//izračunajmo še nekaj statistike
			var povprecje = 0;
			//začnemo z 1, ker je prvi podatek ime label na grafu
			for(var i = 1; i < teze.length; i++){
				povprecje += teze[i];
			}
			console.log(povprecje);
			povprecje /= (teze.length-1);
			document.getElementById("povprecje").innerHTML = "Vaše povprečje je: " + Math.round(povprecje) + " kg";
			
			var bmi = Math.round(povprecje / ((visina/100)*(visina/100)));
			document.getElementById("bmi").innerHTML = "Vaš BMI je: " + bmi;
			document.getElementById("bmiTabela").style.visibility = "visible";
			
			var stanje = "presuh";
			for(var i = 0; i < document.getElementsByTagName("tr").length; i++){
				document.getElementsByTagName("tr")[i].style.border = "0";	
			}
			
			//dodaj funkcionalnost ki predlaga diete in predlaga tek/sprehod
			if(bmi < 16){
				document.getElementById("pod16").style.border = "dotted red";
			}else if(bmi < 18){
				document.getElementById("pod18").style.border = "dotted red";
			}else if(bmi < 25){
				stanje = "normalen";
				document.getElementById("pod25").style.border = "dotted red";
			}else if(bmi < 30){
				stanje = "predebel";
				document.getElementById("pod30").style.border = "dotted red";
			}else{
				stanje = "predebel";
				document.getElementById("nad30").style.border = "dotted red";
			}
			var opisDiete = document.getElementById("opisDiete");
			
			if(stanje == "presuh"){
				opisDiete.innerHTML = "Če težko pridobite na masi, je najbolj pomembno, da povečate število kalorij, ki jih zaužijete. Za postopno pridobivanje kilogramov je dobro na dan zaužiti 300 do 500 kalorij več kot jih sedaj. Pomembno je, da ne začnete uživati velike količine sladkarij in procesirane hrane, temveč povečate uživanje zdrave hrane. Večkrat jejte oreščke, zdrava olja in predvsem povečajte vnos beljakovin, ogljikovih hidratov ter maščob. To pomeni polnozrnate ogljikove hidrate, belo meso, olivno olje, avokado, stročnice, ne pozabite pa seveda na zelenjavo, ki vam bo dala večino vitaminov in mineralov. Ob udejstvovanju s športom obvezno pojejte več, kot porabite in pijte veliko vode. Vsekakor pa pazite, da jeste zajtrk. ";

			}else if(stanje == "normalen"){
				opisDiete.innerHTML = "Gre ti super, kar tako naprej, a ne pozabi na šport, saj je kondicija vedno pomembna."
			}else{
				opisDiete.innerHTML = "Prvo pravilo je, da zaužijete ravno pravo število kalorij. Če sedaj pojeste preveč, jejte manj. Lahko si izračunate porabljene kalorije in pazite na vnešene. Zelo pomembno je, da iz svoje prehrane odstranite vso procesirano hrano, sladkarije, nezdrava rastlinska olja in bel kruh, riž, ter navadne testenine. Vse to lahko zamenjate s sadjem, zdravimi olji in maščobami (olivno olje, avokado), polnozrnatimi testeninami, temnim rižem ter črnim kruhom. Pojejte manj rdečega mesa in se osredotočite na zelenjavo. Poleg tega je bistvenega pomena voda, spijte 2 litra vode na dan, ob telovadbi še več. Če pijete alkohol in veliko število sladkanih pijač, le te odstranite iz diete. Število obrokov povečajte na 5, obvezen je zajtrk, po 19:00 pa ne jejte več. Ne pozabite tudi na šport, ki vam bo hitreje pomagal doseči zastavljene cilje. Poleg naštetega pa ne začnite z dietami, ki se jih ne morete posluževati dlje časa. Če ne morete brez čipsa ga na primer ne odstranite popolnoma, ampak le zmanjšajte količino, ki jo pojeste. Bistveno je, da ob krizi ne zapadete v stara pota. ";

			}
			//da preprečimo, da se graf riše z več istimi podatki (množenje istih podatkov)
			teze = [];
		}, 200);
			}
	}	
	
	

document.getElementById("generiranje").addEventListener("click", function() {
		
		console.log("Sem");
		
		
		generirajPodatke(1);
	    generirajPodatke(2);
	    generirajPodatke(3);
	    	
		setTimeout(function(){

	    	console.log("EHR 1 " + ena);
	    	console.log("EHR 2 " + dva);
	    	console.log("EHR 3 " + tri);
	    
			var enika = document.getElementById("ena");
			var dvoika = document.getElementById("dva");
			var troika = document.getElementById("tri");
		
			enika.value = ena;
			dvoika.value = dva;
			troika.value = tri;
			
			vrniIme(ena)
			vrniIme(dva)
			vrniIme(tri)
			
			setTimeout(function(){
				enika.innerHTML = ime1;
				dvoika.innerHTML = ime2;
				troika.innerHTML = ime3;
				
					$("#obvestiloOZGeneriranju").html("<span class='obvestilo label " +
          "label-success fade-in'>Podatki so se uspešno zgenerirali</span>");
			}, 400);
			
			
		}, 1000);
		
	});
	
    function dobiVreme(){
		var apiURI = "https://api.openweathermap.org/data/2.5/weather?q=Ljubljana&APPID=0ed0f4aa66acaa63bc6d3ebfd4f0007c";

		$.ajax({
    		url: apiURI,
    		dataType: "json",
    		type: "GET",
    		async: "false",
    		success: function(resp) {
    			document.getElementById("vremeNapoved").innerHTML = "Temperatura: " + Math.round((resp.main.temp - 273)) 
    			+ " Vreme: " + resp.weather[0].main;
    			if(resp.weather[0].main != "Rain" && resp.weather[0].main != "Thunderstorm" && resp.weather[0].main != "Snow"){
    				document.getElementById("analizaVremena").innerHTML = "Bi naredili nekaj zase in šli na kratek sprehod ali tek?";
    			}else{
    				document.getElementById("analizaVremena").innerHTML = "Vreme za tek ni najbolj primerno. Kaj če bi namesto tega odšli v fitnes?";
    			}
    		}
    	
    	});
    	
	}
	dobiVreme();
	
	$('#preberiObstojeciEHR').change(function() {
		/*$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
		*/
		var izbira = document.getElementById("preberiObstojeciEHR");
		var ehr = izbira.value;
		document.getElementById("rezultatPoizvedbeOImenu").innerHTML = "Ehr bolnika: " + ehr;
		zavrniPacienta(document.getElementById("preberiObstojeciEHR").value);
		//document.getElementById("rezultatPoizvedbeOImenu").innerHTML = "42";
		
	});
	document.getElementById("gumbZaIzris").addEventListener("click", function(){
		zavrniPacienta(document.getElementById("podatkaIzEhr").value);
	});
	
}



	
